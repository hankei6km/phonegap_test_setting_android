#!/usr/bin/env python
# coding: utf-8
# Copyright (c) 2011 hankei6km
# License: MIT License (http://opensource.org/licenses/mit-license.php)
'''
Created on 2011/10/17
@author: hankei6km


指定されたPhoneGap+Androidプロジェクト（フォルダ）のメインActivityの.javaファイルを特定し、
以下の行のtarget_resourceの部分を指定された文字列で置き換える
 `super.loadUrl("target_resource");`

これにより、作業中のプロジェクトを実行したときにロードされるページを、通常時のページとテスト用のページとで切り替える

Usage: phonegap_test_setting_android.py project_folder replace_with_load_url
'''
import sys
import os
from xml.etree.ElementTree import ElementTree
import re

def get_project_path(proj_rel_path):
    """ プロジェクトの絶対パスを取得する
    """
    return os.path.abspath(proj_rel_path)

def get_android_nanifest_xml_path(proj_path):
    """ proj_path(絶対パス)から AndroidManifest.xml のパスを取得する
    """
    return os.path.join(proj_path, "AndroidManifest.xml")


def chk_argv(argv):
    """ 引数のチェック（数とファイルの存在チェック）。妥当なら True、だめならFalse 
    """
    ret = True
    
    try:
        if len(argv) == 3 :
            # AndroidManifest.xmlが存在するか？
            if os.path.exists(get_android_nanifest_xml_path(get_project_path(argv[1]))) == False:
                ret = False
            #on|offチェック
            if(argv[2] == ""):
                ret = False;
        else:
            ret = False
    except TypeError:
        pass
    finally:
        pass
        
    return ret

def is_accept_package(package):
    """受付できるpackageか？
    受付できるのは "com.example"のようなパターンで、".App" "App"のようなものはFalse
    """
    ret = True
    if package != "":
        p = re.compile(u"^[^\\.]+\\..*$")
        if p.search(package) is None:
            ret = False
    else:
        ret = False
        
    return ret

def is_accept_activity(activity):
    """受付できるactivityか？
    受付できるのは ".App" "App"のようなパターンで、"com.example.App"のようなものはFalse
    """
    ret = True
    if activity != "":
        p = re.compile(u"^[^\\.]+\\..*$")
        if p.search(activity) is not None:
            ret = False
    else:
        ret = False
        
    return ret

def get_activity_path_from_package_and_activity(package, activity):
    """packageとactivityから.javaのパスを取得する。ただし、このパスはプロジェクトのルートフォルダからの相対パス
    """
    ra = re.compile(u"^\\.")
    return os.path.join("src", os.path.join(package.replace(".", os.path.sep), ra.sub(u"", activity))) + os.path.extsep + "java"


def get_activity_path(android_manifest_xml):
    """ AndroidManifest.xmlの内容からメインのActivityのパスを取得する
        取得できなかったらブランクを返す。このときにエラーの内容も返している
    """
    ret = ""
    err = ""

    try:
        package = ""
        xml = ElementTree();
        xml.parse(android_manifest_xml)
        for node in xml.getiterator():
            if node.tag == u"manifest":
                if node.get(u"package") is not None:
                    package = node.get(u"package")
                    break
                
        activity = ""
        for node in xml.findall(u"application/activity"):
            if node.find(u"intent-filter/action") is not None and node.find(u"intent-filter/category") is not None:
                if node.find(u"intent-filter/action").get(u"{http://schemas.android.com/apk/res/android}name") == u"android.intent.action.MAIN" and node.find("intent-filter/category").get("{http://schemas.android.com/apk/res/android}name") == u"android.intent.category.LAUNCHER":
                    activity = node.get(u"{http://schemas.android.com/apk/res/android}name")
            
        if is_accept_package(package) and is_accept_activity(activity):
            ret = get_activity_path_from_package_and_activity(package, activity)
        elif package == "":
            err = u"package is not found"
        elif activity == "":
            err = u"activity is not found"
        elif is_accept_package(package) == False:
            err = u"package is not accept:" + package
        elif is_accept_activity(activity) == False:
            err = u"activity is not accept:" + activity
        else:
            err = u"unknown error"
        
    except:
        err = "get_activity_path exception:" + str(sys.exc_info()[:2])
    finally:
        pass

    if ret == "" and err == "":
        err = u"unknown error"
        
    return ret, err

def replace_load_url(rec_str, replace_with_load_url):
    """レコードにsuper.loadUrl(???);があれば、replace_with_load_urlに置き換える
    """
    r = re.compile(u"(super\\.loadUrl\(\").*?(\"\);)")
    return r.sub("\\1" + replace_with_load_url + "\\2", rec_str);

def replace_activity_file(activity_path, replace_index_html):
    """Activityのソースファイル(activity_path)内のsuper.loadUrl(???);の???部分を置き換える
    """
    replaced_rec = []
    with open(activity_path, "r") as in_file:
        for rec in in_file:
            replaced_rec.append(replace_load_url(rec, replace_index_html))
            
    with open(activity_path, "wb") as out_file:
        for rec in replaced_rec:
            out_file.write(rec)

        
if __name__ == '__main__':

    if chk_argv(sys.argv):
        activity_path, err = get_activity_path(get_android_nanifest_xml_path(sys.argv[1]))
        if err == "":
            activity_path = os.path.join(get_project_path(sys.argv[1]), activity_path);
            if os.path.exists(activity_path):
                replace_activity_file(activity_path, sys.argv[2])
            else:
                sys.stderr.write(u"Activity File is not exist:" + activity_path)
        else:
            sys.stderr.write(err)
    else:
        print u"Usage: phonegap_test_setting_android.py project_folder replace_with_load_url"
        print u"   ex: phonegap_test_setting_android.py PhoneGapProj file:///android_asset/www/tests/index.html"
