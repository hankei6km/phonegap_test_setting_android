# coding: utf-8
'''
Created on 2011/10/17

@author: hankei6km
'''

import os
import unittest
import phonegap_test_setting_android
import shutil
import filecmp

class PhoneGapTestAndroidTestCase(unittest.TestCase):
    def test_get_proj_path(self):
        self.assertEqual(phonegap_test_setting_android.get_project_path("PhoneGapTestCmd"), os.path.abspath("PhoneGapTestCmd"), "project path not match")

    def test_get_android_nanifest_xml_path(self):
        self.assertEqual(phonegap_test_setting_android.get_android_nanifest_xml_path(os.path.abspath("PhoneGapTestCmd")), os.path.abspath("PhoneGapTestCmd/AndroidManifest.xml"), "AndroidManifest.xml path not match")

    def test_chk_argv(self):
        self.assert_(phonegap_test_setting_android.chk_argv(["", "PhoneGapTestCmd", "assets/www/index.html"]), "normal argv fail(on)")
        self.assert_(phonegap_test_setting_android.chk_argv(["", "PhoneGapTestCmd", "assets/www/tests/index.html"]), "normal argv fail(off)")
        self.assert_(phonegap_test_setting_android.chk_argv(["", "PhoneGapTestCmd/", "assets/www/index.html"]), "normal argv fail(path with /)")
        self.assert_(phonegap_test_setting_android.chk_argv(["", "../tests/PhoneGapTestCmd/", "assets/www/index.html"]), "normal argv fail(parent path)")
        self.assert_(phonegap_test_setting_android.chk_argv(["", "PhoneGapTestCmd\\", "assets/www/index.html"]), "normal argv fail(path with \\)")
        self.assertFalse(phonegap_test_setting_android.chk_argv(["", "", ""]), "argv[1] blank")
        self.assertFalse(phonegap_test_setting_android.chk_argv(["", "test/PhoneGapTestCmd", ""]), "argv[2] blank")
        self.assertFalse(phonegap_test_setting_android.chk_argv(["", "test/PhoneGapTestCmd1", ""]), "argv[1] invalid")

    def test_is_accept_package(self):
        self.assertFalse(phonegap_test_setting_android.is_accept_package(""), "blank")
        self.assertFalse(phonegap_test_setting_android.is_accept_package(".App"), ".App")
        self.assertFalse(phonegap_test_setting_android.is_accept_package("App"), "App")
        self.assert_(phonegap_test_setting_android.is_accept_package("com.example"), "com.example")
        self.assert_(phonegap_test_setting_android.is_accept_package("com.example."), "com.example.")
        self.assert_(phonegap_test_setting_android.is_accept_package("com.example.App"), "com.example.App")

    def test_is_accept_activity(self):
        self.assertFalse(phonegap_test_setting_android.is_accept_activity(""), "blank")
        self.assert_(phonegap_test_setting_android.is_accept_activity(".App"), ".App")
        self.assert_(phonegap_test_setting_android.is_accept_activity("App"), "App")
        self.assertFalse(phonegap_test_setting_android.is_accept_activity("com.example"), "com.example")
        self.assertFalse(phonegap_test_setting_android.is_accept_activity("com.example."), "com.example.")
        self.assertFalse(phonegap_test_setting_android.is_accept_activity("com.example.App"), "com.example.App")

    def test_get_activity_path_from_package_and_activity(self):
        self.assertEqual(phonegap_test_setting_android.get_activity_path_from_package_and_activity("com.example", ".App"), os.path.join("src", os.path.join("com.example".replace(".", os.path.sep), "App")) + os.path.extsep + "java", "com.example and .App")
        self.assertEqual(phonegap_test_setting_android.get_activity_path_from_package_and_activity("com.example", "App"), os.path.join("src", os.path.join("com.example".replace(".", os.path.sep), "App")) + os.path.extsep + "java", "com.example and App")

    def test_get_activity_path(self):
        activity_path = phonegap_test_setting_android.get_activity_path_from_package_and_activity("com.fc2.blog23.zawakei.test.phonegaptestcmd", ".App")
        
        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, activity_path, "normal xml ret:" + ret)
        self.assertEqual(err, "", "normal xml err:" + err)

        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd_invalid_AndroidManifest/Manifest_Is_Not_Exist")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, "", "manifest is not exist xml ret:" + ret)
        self.assertEqual(err, u"package is not found", "manifest is not exist  xml err:" + err)

        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd_invalid_AndroidManifest/Package_Invalid")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, "", "package invalid xml ret:" + ret)
        self.assertEqual(err, u"package is not accept:com", "package invalid xml err:" + err)

        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd_invalid_AndroidManifest/Activity_Invalid")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, "", "activity invalid xml ret:" + ret)
        self.assertEqual(err, u"activity is not accept:com.fc2.blog23.zawakei.test.phonegaptestrun.Test", "activity invalid xml err:" + err)

        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd_invalid_AndroidManifest/Package_Is_Not_Exist")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, "", "package is not exist xml ret:" + ret)
        self.assertEqual(err, u"package is not found", "package is not exist  xml err:" + err)

        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd_invalid_AndroidManifest/Activity_Is_Not_Exist")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, "", "activity is not exist xml ret:" + ret)
        self.assertEqual(err, u"activity is not found", "activity is not exist  xml err:" + err)

        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd_invalid_AndroidManifest/Intent_Not_Match")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, "", "intent not match ret:" + ret)
        self.assertEqual(err, u"activity is not found", "intent not match err:" + err)

        android_nanifest_xml_path = phonegap_test_setting_android.get_android_nanifest_xml_path("PhoneGapTestCmd_invalid_AndroidManifest/Xml_Invalid")
        ret, err = phonegap_test_setting_android.get_activity_path(android_nanifest_xml_path)
        self.assertEqual(ret, "", "xml invalid ret:" + ret)
        self.assertRegexpMatches(err, r"^get_activity_path exception:", "xml invalid err:" + err)

    def test_replace_load_url(self):
        self.assertEqual(phonegap_test_setting_android.replace_load_url('        super.loadUrl("file:///android_asset/www/tests/index.html");', "file:///android_www/asset/index.html"), '        super.loadUrl("file:///android_www/asset/index.html");', "replace file:///android_www/asset/index.html")
        self.assertEqual(phonegap_test_setting_android.replace_load_url('        super.loadUrl("file:///android_asset/www/index.html");', "file:///android_www/asset/tests/index.html"), '        super.loadUrl("file:///android_www/asset/tests/index.html");', "replace file:///android_www/asset/tests/index.html")
        self.assertEqual(phonegap_test_setting_android.replace_load_url("", "replace"), "", "blank rec")
        self.assertEqual(phonegap_test_setting_android.replace_load_url("public class App extends DroidGap {", "replace"), "public class App extends DroidGap {", "ext rec1")
        self.assertEqual(phonegap_test_setting_android.replace_load_url("    @Override", "replace"), "    @Override", "etc rec2")
        

    def test_replace_activity_file(self):
        #元になるファイルからテスト用の作業ファイルにコピー
        shutil.copyfile(os.path.abspath("PhoneGapTestCmd_replace_activity/App_org.java"), os.path.abspath("PhoneGapTestCmd_replace_activity/App.java"))
        
        work_file = os.path.abspath("PhoneGapTestCmd_replace_activity/App.java")
        test_on_file=os.path.abspath("PhoneGapTestCmd_replace_activity/App_test_on.java")
        test_off_file=os.path.abspath("PhoneGapTestCmd_replace_activity/App_test_off.java")

        phonegap_test_setting_android.replace_activity_file(work_file, "file:///android_asset/www/tests/index.html")
        self.assert_(filecmp.cmp(test_on_file, work_file, False), "test_on first")

        phonegap_test_setting_android.replace_activity_file(work_file, "file:///android_asset/www/index.html")
        self.assert_(filecmp.cmp(test_off_file, work_file, False), "test_off first")

        phonegap_test_setting_android.replace_activity_file(work_file, "file:///android_asset/www/tests/index.html")
        self.assert_(filecmp.cmp(test_on_file, work_file, False), "test_on second")

        phonegap_test_setting_android.replace_activity_file(work_file, "file:///android_asset/www/index.html")
        self.assert_(filecmp.cmp(test_off_file, work_file, False), "test_off second")

        #作業ファイル削除
        os.remove(work_file)