phonegap_test_setting_android
==============================

概要
----------------

指定されたPhoneGap+Androidプロジェクト（フォルダ）のメインActivityの.javaファイルを特定し、 `super.loadUrl("target_resource");` のtarget_resourceの部分を指定された文字列で置き換えるスクリプトです。

これにより、作業中のプロジェクトを実行したときにロードされるページを、通常時のページとテスト用のページとで切り替えることができます。


ダウンロード
----------------

ダウンロードタブからeggファイルをダウンロードできます。


インストール
----------------

ダウンロードしたeggファイルをesay_installでインストールできます。


アンインストール
----------------

pipコマンドで  `pip uninstall phonegap_test_setting_android` のようにしてアンインストールできます。


利用方法
----------------

作業中のプロジェクトのフォルダが `PhoneGapProj` で、通常時にロードされるindex.htmlが`assets/www/index.html`、テスト用のindex.htmlが `assets/www/tests/index.html` に配置されていたとします。この場合、プロジェクトを実行したときに、テストを行うようにしたい場合は、コマンドプロンプトから `phonegap_test_setting_android.py PhoneGapProj file:///android_asset/www/tests/index.html` のように、プロジェクトのフォルダ名と置き換えたい文字列を指定して実行します。また、元に戻す場合は、 `phonegap_test_setting_android.py PhoneGapProj file:///android_asset/www/index.html` のように実行します（バージョン管理システムを利用している場合は、対象となるActivityのjavaファイルを１つ前の状態に戻す方が無難かもしれません）。

なお、Eclipse上のプロジェクトの場合は、Activiy用のjavaファイルが変更されたことを検出させるために、ワークスペースの設定を変更する必要があります。これについては、Eclipseのメニュから `Window/Preferences` を選択し`General/Workspace` を開き、 `Refresh using native hooks or polling` にチェックを付けることで設定できます(この設定を変更していない場合は、実行時にJavaファイルの変更が反映されない、Project Ecplorer上のjavaファイルアイコンにバージョン管理システムによる変更表示が反映されない、といった状態になります)


注意点
----------------

- プロジェクトに含まれるファイルを破壊してしまうかもしれませんので、置き換えを実行するときには、以前の状態に戻せるようにしておいてください。
- Activiy用のjavaファイルのタイムスタンプは変更されます。
- 利用されているバージョン管理システムによっては、通常時のjavaファイルに戻しても変更が検出されたりエラーが出るかもしれません。
- バージョン管理システムのコミット作業をする場合、変更したままのjavaファイルをコミットしてしまわないように注意してください。
- Activityのandroid:name属性は `.App` または `App`のように指定されている必要があります(他の名前でも大丈夫ですが、`com.fc2.blog23.zawakei.test.phonegaptestrun.App`のような形式ではエラーになります)。
- 対象となるActivty(判定はintent-filterの定義で行っています)が複数定義されていた場合、その時点で最初に見つかった方のみ置き換えます。
- Eclipse側での変更検出がときどきうまくいかないことがあるようです。その場合は、少し時間をおいて再度実行してみてください。それでもだめなときは対象となるActivityのjavaファイルをEclipseのエディタで開いてみてください(Resource is out of sync with the file systemのような表示が出たらF5でリフレッシュしてください)。
- ワークスペースの設定を変更したことで、Eclipseの操作レスポンスが悪くなるかもしれません。
- 動作確認はWindows+Eclipse+EGitでのみ行っています。他のプラットフォーム上では動作しないかもしれません。
