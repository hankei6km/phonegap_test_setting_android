#!/usr/bin/env python

from setuptools import setup, find_packages
setup(
    name="phonegap_test_setting_android",
    version="0.1.0",
    author='hankei6km',
    license='MIT License',
    description='Turn on/off project test setting(replace loadUrl) for PhoneGap Android',
    url='http://hankei6km.bitbucket.org/',
    scripts=['src/phonegap_test_setting_android.py']
)
